var request = require('request');

request('https://api-test.projectcamp.us/courses/course-1/users', function (error, response, body) {
  if (!error && response.statusCode == 200) {
   //console.log(body)
   console.log(typeof(body));
   var users = JSON.parse(body);
   console.log(users.length);
   current_number_users = users.length;
  }
});

var current_valuation = 0;
var current_karma = 0;
var current_number_users = 0;

setInterval(function() {
  var last_valuation = current_valuation;
  var last_karma = current_karma;
  current_valuation = Math.floor(Math.random() * 100);
  current_karma = Math.floor(Math.random() * 200000);
  var last_number_users = current_number_users;

  send_event('valuation', {current: current_valuation, last: last_valuation});
  send_event('users', {current: current_number_users, last: last_number_users});
  send_event('karma', {current: current_karma, last: last_karma});
  send_event('synergy', {value: Math.floor(Math.random() * 100)});
}, 2 * 1000);
	

